﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ragsack
{
    public class Allocator
    {
        public void Allocate(TalksTimeBox talksTimeBox, List<Talk> proposedTalks)
        {
            talksTimeBox.AllocatedTalks = proposedTalks;
        }
    }
}
