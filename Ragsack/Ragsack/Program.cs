﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ragsack
{
    class Program
    {
        static void Main(string[] args)
        {
            var morningSession = new MorningSession(new Allocator());
            var eveningSession = new EveningSession(new Allocator());
            var lunchBreak = new LunchBreak();
            var networkingEvent = new LunchBreak();

            List<TimeBox> timeBoxes = new List<TimeBox>()
            {
                morningSession, eveningSession, lunchBreak, networkingEvent
            };

            //var track1 = new Track();
            //track1.AddTimeBox(morningSession);
            //track1.AddTimeBox(eveningSession);
            //track1.AddTimeBox(lunchBreak);
            //track1.AddTimeBox(networkingEvent);

            //var track2 = new Track();
            //track2.AddTimeBox(morningSession);
            //track2.AddTimeBox(eveningSession);
            //track2.AddTimeBox(lunchBreak);
            //track2.AddTimeBox(networkingEvent);

            var talk = new Talk("", 1, TalkType.Normal);
            var proposedTalks = new List<Talk>();
            proposedTalks.Add(talk);           

            //morningSession.AllocateTalks(proposedTalks);

            //var allocatedTalks = morningSession.AllocatedTalks;
            //proposedTalks.RemoveAll(n => morningSession.AllocatedTalks.Contains(n));

            var Tracks = new List<Track>();        
            int proposedTalksCount = proposedTalks.Count;

            while (proposedTalksCount > 0)
            {
                Track track = new Track();

                foreach(TimeBox timeBox in timeBoxes)
                {
                    track.AddTimeBox(timeBox);
                }

                foreach (TimeBox timeBox in track.TimeBoxes)
                {
                    timeBox.AllocateTalks(proposedTalks);

                    proposedTalks.RemoveAll(n => timeBox.AllocatedTalks.Contains(n));
                }
                Tracks.Add(track);
                proposedTalksCount = proposedTalks.Count;
            }
        }
    }
}
