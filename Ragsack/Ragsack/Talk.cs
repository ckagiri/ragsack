﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ragsack
{
    public enum TalkType
    {
        Normal,
        Lightning
    }

    public class Talk
    {
        public string Name { get; private set; }
        public decimal Minutes { get; private set; }
        public TalkType TalkType { get; private set; }

        public Talk(string name, decimal minutes, TalkType talkType)
        {
            Name = name;
            Minutes = minutes;
            TalkType = TalkType;
        }
    }
}
