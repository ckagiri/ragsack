﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ragsack
{
    public abstract class TimeBox
    {
        public TimeBox()
        {
            AllocatedTalks = new List<Talk>();
        }

        public List<Talk> AllocatedTalks { get; set; }

        public abstract void AllocateTalks(List<Talk> talks);
    }
}
