﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ragsack
{
    public class Track
    {
        public List<TimeBox> TimeBoxes { get; set; } = new List<TimeBox>();

        public void AddTimeBox(TimeBox timeBox)
        {
            TimeBoxes.Add(timeBox);
        }
    }
}
