﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ragsack
{
    public abstract class TalksTimeBox : TimeBox
    {
        private Allocator _allocator;

        public TalksTimeBox(Allocator allocator) : base()
        {
            _allocator = allocator;
        }

        public override void AllocateTalks(List<Talk> proposedTalks)
        {
            _allocator.Allocate(this, proposedTalks);
        }
    }
}
