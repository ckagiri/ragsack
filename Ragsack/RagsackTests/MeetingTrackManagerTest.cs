﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ragsack;
using System.Collections.Generic;

namespace RagsackTests
{
    [TestClass]
    public class MeetingTrackManagerTest
    {
        [TestMethod]
        public void ItShouldAllocateCorrectly()
        {
            var proposedTalks = new List<Talk>()
            {
                new Talk("Writing Fast Tests Against Enterprise Rails", 60, TalkType.Normal)
            };
            Allocator allocator = new Allocator();
            MeetingTrackManager app = new MeetingTrackManager(allocator);
            List<Talk> allocatedTalks = app.AllocateTracks(proposedTalks);

            Assert.AreEqual(2, allocatedTalks.Count);
        }
    }
}
